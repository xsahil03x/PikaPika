package com.floydwiz.pikapika;

import com.floydwiz.lockpatternview.SpUtil;
import com.floydwiz.pikapika.di.DaggerAppComponent;
import com.floydwiz.pikapika.utils.PikaPikaLogger;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class PikaPikaApp extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        PikaPikaLogger.init();
        SpUtil.getInstance().init(this);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(PikaPikaApp.this).build();
    }
}
