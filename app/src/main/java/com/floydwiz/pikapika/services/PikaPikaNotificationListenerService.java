package com.floydwiz.pikapika.services;

import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import com.floydwiz.pikapika.utils.PikaPikaLogger;

public class PikaPikaNotificationListenerService extends NotificationListenerService {

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        PikaPikaLogger.i("onNotificationRemoved() called with: sbn = [" + sbn + "]");
        /*String packageName = SharedPrefHelper.getSharedPreferenceString(getApplicationContext(), PACKAGENAME_KEY, "");
        if (SharedPrefHelper.getSharedPreferenceBoolean(this, CHILDMODE_KEY, false) && sbn.getPackageName().equalsIgnoreCase(packageName)) {
            Log.d(TAG, "onNotificationPosted: " + sbn.getPackageName());
            cancelNotification(sbn.getKey());
        }*/
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
        PikaPikaLogger.i("onNotificationRemoved() called with: sbn = [" + sbn + "]");
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        PikaPikaLogger.i("onListenerConnected() called");
    }

    @Override
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
        PikaPikaLogger.i("onListenerDisconnected() called");
    }

    @Override
    public StatusBarNotification[] getActiveNotifications() {
        PikaPikaLogger.i("getActiveNotifications() called");
        return super.getActiveNotifications();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PikaPikaLogger.i("onDestroy() called");
    }
}
