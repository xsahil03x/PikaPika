package com.floydwiz.pikapika.ui.launcher.parent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.data.models.AllowedApp;
import com.floydwiz.pikapika.databinding.FragmentParentBinding;
import com.floydwiz.pikapika.ui.base.BaseFragment;
import com.floydwiz.pikapika.ui.launcher.AppClickListener;
import com.floydwiz.pikapika.ui.launcher.LauncherViewModel;
import com.floydwiz.pikapika.utils.ApkInfoExtractor;
import com.floydwiz.pikapika.utils.AppUtils;
import com.floydwiz.pikapika.utils.GridSpacingItemDecoration;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ParentFragment extends BaseFragment<LauncherViewModel, FragmentParentBinding> implements AppClickListener {

    @Inject
    ApkInfoExtractor extractor;
    private RecyclerView allAppRecyclerview;
    private AllAppsAdapter adapter;

    @Override
    protected int provideLayout() {
        return R.layout.fragment_parent;
    }

    @Override
    protected Class<LauncherViewModel> provideViewModelClass() {
        return LauncherViewModel.class;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        allAppRecyclerview = getDataBinding().rvAllApps;
        adapter = new AllAppsAdapter(extractor, this);
        allAppRecyclerview.setLayoutManager(new GridLayoutManager(getContext(), 3));
        allAppRecyclerview.addItemDecoration(new GridSpacingItemDecoration(3, AppUtils.dpToPx(20), true));
        allAppRecyclerview.setItemAnimator(new DefaultItemAnimator());
        allAppRecyclerview.setAdapter(adapter);

        getViewModel().fetchParentAppList().observe(this, allowedAppsList -> adapter.addAppsToList(allowedAppsList));
    }

    @Override
    public void onAppClicked(AllowedApp app, int position) {
        getViewModel().addAppToKidShell(app);
        adapter.notifyItemChanged(position);
    }
}
