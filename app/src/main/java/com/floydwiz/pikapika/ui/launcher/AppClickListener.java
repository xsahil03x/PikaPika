package com.floydwiz.pikapika.ui.launcher;

import com.floydwiz.pikapika.data.models.AllowedApp;

public interface AppClickListener {

    void onAppClicked(AllowedApp apps, int position);

}
