package com.floydwiz.pikapika.ui.launcher.parent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.data.models.AllowedApp;
import com.floydwiz.pikapika.databinding.AppParentItemBinding;
import com.floydwiz.pikapika.di.modules.GlideApp;
import com.floydwiz.pikapika.ui.launcher.AppClickListener;
import com.floydwiz.pikapika.utils.ApkInfoExtractor;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import static com.floydwiz.pikapika.utils.AppUtils.dpToPx;
import static com.floydwiz.pikapika.utils.AppUtils.getScreenWidth;

public class AllAppsAdapter extends RecyclerView.Adapter<AllAppsAdapter.AppViewHolder> {

    private ApkInfoExtractor extractor;
    private AppClickListener appClickListener;
    private List<AllowedApp> mApps;

    public AllAppsAdapter(ApkInfoExtractor extractor, AppClickListener appClickListener) {
        this.extractor = extractor;
        this.appClickListener = appClickListener;
    }

    public void addAppsToList(List<AllowedApp> appsList) {
        this.mApps = appsList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AppViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AppParentItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.app_parent_item, parent, false);
        binding.getRoot().getLayoutParams().width = (getScreenWidth(binding.ivAppLogo.getContext()) / 4);
        binding.getRoot().getLayoutParams().height = (getScreenWidth(binding.ivAppLogo.getContext()) / 4);
        binding.ivAppLogo.getLayoutParams().width = (getScreenWidth(binding.ivAppLogo.getContext()) / 4) - dpToPx(32);
        binding.ivAppLogo.getLayoutParams().height = (getScreenWidth(binding.ivAppLogo.getContext()) / 4) -dpToPx(32);
        return new AppViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final AppViewHolder holder, int position) {
        holder.mBinding.setApp(mApps.get(position));
        GlideApp.with(holder.mBinding.ivAppLogo.getContext())
                .load(extractor.getAppIconByPackageName(mApps.get(position).getPackageName()))
                .dontAnimate()
                .into(holder.mBinding.ivAppLogo);
    }

    @Override
    public int getItemCount() {
        if (mApps == null) {
            return 0;
        } else {
            return mApps.size();
        }
    }

    class AppViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final AppParentItemBinding mBinding;

        AppViewHolder(AppParentItemBinding itemBinding) {
            super(itemBinding.getRoot());
            this.mBinding = itemBinding;
            mBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            appClickListener.onAppClicked(mApps.get(getAdapterPosition()), getAdapterPosition());
        }
    }
}
