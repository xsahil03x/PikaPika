package com.floydwiz.pikapika.ui.onboarding;

import android.os.Bundle;

import com.floydwiz.pikapika.R;

import dagger.android.support.DaggerAppCompatActivity;

public class OnBoardingActivity extends DaggerAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
    }

}
