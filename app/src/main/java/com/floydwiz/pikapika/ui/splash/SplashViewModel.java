package com.floydwiz.pikapika.ui.splash;

import com.floydwiz.pikapika.data.local.prefs.AppPreferencesHelper;
import com.floydwiz.pikapika.ui.base.BaseViewModel;

import javax.inject.Inject;

public class SplashViewModel extends BaseViewModel {

    private AppPreferencesHelper helper;

    @Inject
    SplashViewModel(AppPreferencesHelper helper) {
        this.helper = helper;
    }

    boolean isUserRegistered() {
//        return true;
        return helper.getUserLoggedIn() && helper.getSelfLockState() && helper.getPermissionsState();
    }
}
