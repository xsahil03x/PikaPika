package com.floydwiz.pikapika.ui.launcher;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floydwiz.lockpatternview.LockConstants;
import com.floydwiz.lockpatternview.LockPatternUtils;
import com.floydwiz.lockpatternview.LockPatternView;
import com.floydwiz.lockpatternview.LockPatternViewPattern;
import com.floydwiz.lockpatternview.SpUtil;
import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.databinding.FragmentSelfUnlockBinding;
import com.floydwiz.pikapika.utils.HomeUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class SelfUnlockFragment extends Fragment {

    private FragmentSelfUnlockBinding mBinding;
    private LockPatternView mLockPatternView;
    private LockPatternUtils mLockPatternUtils;
    private int mFailedPatternAttemptsSinceLastTimeout = 0;
    private NavController navController;
    private Boolean isClose;
    @NonNull
    private Runnable mClearPatternRunnable = new Runnable() {
        public void run() {
            mLockPatternView.clearPattern();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_self_unlock, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLockPatternView = mBinding.lockPatternView;
        navController = Navigation.findNavController(mBinding.getRoot());
        isClose = getArguments().getBoolean("isClose", false);
        if (isClose)
            mBinding.ivHeader.setImageDrawable(getResources().getDrawable(R.drawable.header_close_app));
        initLockPatternView();
    }

    private void initLockPatternView() {
        mLockPatternUtils = new LockPatternUtils(getContext());
        LockPatternViewPattern mPatternViewPattern = new LockPatternViewPattern(mLockPatternView);
        mPatternViewPattern.setPatternListener(pattern -> {
            if (mLockPatternUtils.checkPattern(pattern)) {
                mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Correct);
                if (!isClose)
                    navController.navigate(R.id.selfUnlockFragment_to_parentFragment);
                else
                    HomeUtil.disablePikaPikaLauncher(getContext(), getContext().getPackageManager());
            } else {
                mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                if (pattern.size() >= LockPatternUtils.MIN_PATTERN_REGISTER_FAIL) {
                    mFailedPatternAttemptsSinceLastTimeout++;
                    int retry = LockPatternUtils.FAILED_ATTEMPTS_BEFORE_TIMEOUT - mFailedPatternAttemptsSinceLastTimeout;
                    if (retry >= 0) {

                    }
                } else {

                }
                if (mFailedPatternAttemptsSinceLastTimeout >= 3) {
                    if (SpUtil.getInstance().getBoolean(LockConstants.LOCK_AUTO_RECORD_PIC, false)) {

                    }
                }
                if (mFailedPatternAttemptsSinceLastTimeout >= LockPatternUtils.FAILED_ATTEMPTS_BEFORE_TIMEOUT) { //The number of failures is greater than the maximum number of incorrect attempts before blocking the use

                } else {
                    mLockPatternView.postDelayed(mClearPatternRunnable, 500);
                }
            }
        });
        mLockPatternView.setOnPatternListener(mPatternViewPattern);
        mLockPatternView.setTactileFeedbackEnabled(true);
    }
}
