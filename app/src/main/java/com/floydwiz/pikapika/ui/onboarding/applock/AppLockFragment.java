package com.floydwiz.pikapika.ui.onboarding.applock;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.floydwiz.lockpatternview.GestureCreateContract;
import com.floydwiz.lockpatternview.GestureCreatePresenter;
import com.floydwiz.lockpatternview.LockPatternUtils;
import com.floydwiz.lockpatternview.LockPatternView;
import com.floydwiz.lockpatternview.LockPatternViewPattern;
import com.floydwiz.lockpatternview.LockStage;
import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.data.local.prefs.AppPreferencesHelper;
import com.floydwiz.pikapika.databinding.FragmentAppLockBinding;
import com.floydwiz.pikapika.utils.HomeUtil;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import dagger.android.support.DaggerFragment;

public class AppLockFragment extends DaggerFragment implements View.OnClickListener,
        GestureCreateContract.View {

    private FragmentAppLockBinding mBinding;

    @Inject
    AppPreferencesHelper mHelper;

    @Nullable
    private List<LockPatternView.Cell> mChosenPattern = null;

    private TextView mLockTip;
    private LockPatternView mLockPatternView;
    @NonNull
    private final Runnable mClearPatternRunnable = new Runnable() {
        public void run() {
            mLockPatternView.clearPattern();
        }
    };
    private LockStage mLockStage = LockStage.Introduction;
    private LockPatternUtils mLockPatternUtils;
    private GestureCreatePresenter mGestureCreatePresenter;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_app_lock, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mHelper.getSelfLockState()) gotoLauncherActivity();
        mLockPatternView = mBinding.lockPatternView;
        mLockTip = mBinding.txtLockTip;
        Button btnReset = mBinding.btnReset;
        btnReset.setOnClickListener(this);
        mGestureCreatePresenter = new GestureCreatePresenter(this, getContext());
        initLockPatternView();
    }

    private void initLockPatternView() {
        mLockPatternUtils = new LockPatternUtils(getContext());
        LockPatternViewPattern mPatternViewPattern = new LockPatternViewPattern(mLockPatternView);
        mPatternViewPattern.setPatternListener(pattern -> mGestureCreatePresenter.onPatternDetected(pattern, mChosenPattern, mLockStage));
        mLockPatternView.setOnPatternListener(mPatternViewPattern);
        mLockPatternView.setTactileFeedbackEnabled(true);
    }

    @Override
    public void onClick(@NonNull View view) {
        switch (view.getId()) {
            case R.id.btnReset:
                setStepOne();
                break;
        }
    }

    private void setStepOne() {
        mGestureCreatePresenter.updateStage(LockStage.Introduction);
        mLockTip.setText(getString(R.string.lock_recording_intro_header));
        mBinding.ivHeader.setImageDrawable(getResources().getDrawable(R.drawable.header_applock));
    }

    private void gotoLauncherActivity() {
//        BackgroundManager.getInstance().init(this).startService(LockService.class);
        HomeUtil.handleHomeButton(getContext(),new int[0]);
        getActivity().finish();
    }

    @Override
    public void updateUiStage(LockStage stage) {
        mLockStage = stage;
    }

    @Override
    public void updateChosenPattern(List<LockPatternView.Cell> mChosenPattern) {
        this.mChosenPattern = mChosenPattern;
    }

    @Override
    public void updateLockTip(String tip, boolean isToast) {
        mLockTip.setText(tip);
    }

    @Override
    public void setHeaderMessage(int headerMessage) {
        if (headerMessage == com.floydwiz.lockpatternview.R.string.lock_need_to_confirm) {
            mBinding.ivHeader.setImageDrawable(getResources().getDrawable(R.drawable.header_applock_again));
        }
        mLockTip.setText(headerMessage);
    }

    @Override
    public void lockPatternViewConfiguration(boolean patternEnabled, LockPatternView.DisplayMode displayMode) {
        if (patternEnabled)
            mLockPatternView.enableInput();
        else
            mLockPatternView.disableInput();

        mLockPatternView.setDisplayMode(displayMode);
    }

    @Override

    public void Introduction() {
        clearPattern();
    }

    @Override
    public void HelpScreen() {

    }

    @Override
    public void ChoiceTooShort() {
        mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
        mLockPatternView.removeCallbacks(mClearPatternRunnable);
        mLockPatternView.postDelayed(mClearPatternRunnable, 500);
    }

    @Override
    public void moveToStatusTwo() {

    }

    @Override
    public void clearPattern() {
        mLockPatternView.clearPattern();
    }

    @Override
    public void ConfirmWrong() {
        mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
        mLockPatternView.removeCallbacks(mClearPatternRunnable);
        mLockPatternView.postDelayed(mClearPatternRunnable, 500);
    }

    @Override
    public void ChoiceConfirmed() {
        mLockPatternUtils.saveLockPattern(mChosenPattern);
        clearPattern();
        mHelper.setSelfLockState(true);
        gotoLauncherActivity();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mGestureCreatePresenter.onDestroy();
    }
}
