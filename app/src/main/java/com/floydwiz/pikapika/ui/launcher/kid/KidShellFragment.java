package com.floydwiz.pikapika.ui.launcher.kid;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.data.models.AllowedApp;
import com.floydwiz.pikapika.databinding.FragmentKidShellBinding;
import com.floydwiz.pikapika.ui.base.BaseFragment;
import com.floydwiz.pikapika.ui.launcher.AppClickListener;
import com.floydwiz.pikapika.ui.launcher.LauncherViewModel;
import com.floydwiz.pikapika.utils.ApkInfoExtractor;
import com.floydwiz.pikapika.utils.HomeUtil;
import com.floydwiz.pikapika.utils.LauncherType;
import com.floydwiz.pikapika.utils.SnapToBlock;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import javax.inject.Inject;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class KidShellFragment extends BaseFragment<LauncherViewModel, FragmentKidShellBinding> implements AppClickListener, OnBackPressedCallback {

    @Inject
    ApkInfoExtractor extractor;
    private RecyclerView allAppRecyclerview;
    private KidAppsAdapter adapter;
    private MoreOptionDialog moreOptionDialog;

    private static final int LAUNCHER_RESPONSE = 33;

    private static final String TAG = "KidShellFragment";

    @Override
    protected int provideLayout() {
        return R.layout.fragment_kid_shell;
    }

    @Override
    protected Class<LauncherViewModel> provideViewModelClass() {
        return LauncherViewModel.class;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkIfLauncherIsDefault();
        final NavController navController = Navigation.findNavController(getDataBinding().getRoot());

        // Add callback for onBackPressed in fragment
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), this);

        moreOptionDialog = new MoreOptionDialog(getContext(), navController);
        allAppRecyclerview = getDataBinding().rvKidApps;
        adapter = new KidAppsAdapter(extractor, this);
        allAppRecyclerview.setLayoutManager(new GridLayoutManager(getContext(), 3, GridLayoutManager.HORIZONTAL, false));
        allAppRecyclerview.setItemAnimator(new DefaultItemAnimator());
        allAppRecyclerview.setAdapter(adapter);
        SnapToBlock snapToBlock = new SnapToBlock(3);
        snapToBlock.attachToRecyclerView(allAppRecyclerview);

        getViewModel().fetchKidsAppList().observe(this, allowedAppsList -> adapter.addAppsToList(allowedAppsList));

        getDataBinding().ivMore.setOnClickListener(v -> showDialog());
    }

    private void checkIfLauncherIsDefault() {
        if (HomeUtil.getLauncherType(getContext()) != LauncherType.IS_DEFAULT) {
            new MaterialAlertDialogBuilder(getContext())
                    .setTitle("Set default launcher")
                    .setMessage("For best experience for your Kid select \"PikaPika\" as default")
                    .setPositiveButton("Ok", (dialog, which) -> {
                        openLauncherDialog();
                    })
                    .show();
        }
    }

    private void showDialog() {
        WindowManager.LayoutParams params = moreOptionDialog.getWindow().getAttributes();
        params.gravity = Gravity.TOP | Gravity.END;
        params.x = 100;
        params.y = 100;
        moreOptionDialog.show();
    }

    private void openLauncherDialog() {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        startActivityForResult(intent, LAUNCHER_RESPONSE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
    }

    @Override
    public void onAppClicked(AllowedApp apps, int position) {
        Intent launchGame = getContext().getPackageManager().getLaunchIntentForPackage(apps.getPackageName());
        startActivity(launchGame);
    }

    @Override
    public boolean handleOnBackPressed() {
        return true;
    }

}
