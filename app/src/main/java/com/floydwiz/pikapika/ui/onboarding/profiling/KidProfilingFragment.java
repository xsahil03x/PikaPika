package com.floydwiz.pikapika.ui.onboarding.profiling;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.data.local.prefs.AppPreferencesHelper;
import com.floydwiz.pikapika.data.models.User;
import com.floydwiz.pikapika.databinding.FragmentKidProfilingBinding;
import com.floydwiz.pikapika.ui.base.BaseFragment;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;


/**
 * A simple {@link Fragment} subclass.
 */
public class KidProfilingFragment extends BaseFragment<ProfilingViewModel, FragmentKidProfilingBinding> {

    @Inject
    AppPreferencesHelper mHelper;

    @Override
    protected int provideLayout() {
        return R.layout.fragment_kid_profiling;
    }

    @Override
    protected Class<ProfilingViewModel> provideViewModelClass() {
        return ProfilingViewModel.class;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final NavController navController = Navigation.findNavController(getDataBinding().getRoot());

        if (mHelper.getUserLoggedIn())
            navController.navigate(R.id.kidProfilingFragment_to_permissionsAccessFragment);

        getDataBinding().setProfilingViewModel(getViewModel());
        getViewModel().getButtonClick().observe(this, profilingFields -> {
            User user = new User(
                    UUID.randomUUID().toString(),
                    profilingFields.getChildName(),
                    profilingFields.getChildAge(),
                    profilingFields.getParentEmail()
            );
            ProgressDialog dialog = new ProgressDialog(getContext()); // this = YourActivity
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setTitle("Registering...");
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            getViewModel().createNewUser(user).observe(this, statusCode -> {
                if (statusCode == 200) {
                    dialog.dismiss();
                    mHelper.setUserLoggedIn(true);
                    navController.navigate(R.id.kidProfilingFragment_to_permissionsAccessFragment);
                } else Toast.makeText(getContext(), "Some Error", Toast.LENGTH_SHORT).show();
            });
        });
    }
}
