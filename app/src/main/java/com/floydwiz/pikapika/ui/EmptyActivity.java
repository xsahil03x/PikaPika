package com.floydwiz.pikapika.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.floydwiz.pikapika.R;

public class EmptyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empty);
    }
}
