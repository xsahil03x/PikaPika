package com.floydwiz.pikapika.ui.launcher;

import com.floydwiz.pikapika.data.AppRepository;
import com.floydwiz.pikapika.data.models.AllowedApp;
import com.floydwiz.pikapika.ui.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;

public class LauncherViewModel extends BaseViewModel {

    private AppRepository appRepository;

    @Inject
    LauncherViewModel(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    public LiveData<List<AllowedApp>> fetchParentAppList() {
        return appRepository.getAllAppList();
    }

    public LiveData<List<AllowedApp>> fetchKidsAppList() {

        return appRepository.getAllowedAppList();
    }

    public void addAppToKidShell(AllowedApp app) {
        appRepository.handleSelectedApp(app);
    }

}
