package com.floydwiz.pikapika.ui.launcher.kid;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.floydwiz.pikapika.R;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;

public class MoreOptionDialog extends Dialog implements View.OnClickListener {

    private Button btnClose, btnParentMode;
    private NavController navController;

    MoreOptionDialog(@NonNull Context context, NavController navController) {
        super(context);
        this.navController = navController;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCanceledOnTouchOutside(true);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.layout_options_dialog);

        btnClose = findViewById(R.id.btnClose);
        btnParentMode = findViewById(R.id.btnParentShell);

        btnClose.setOnClickListener(this);
        btnParentMode.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        final Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.btnClose:
                bundle.putBoolean("isClose", true);
                navController.navigate(R.id.kidShellFragment_to_selfUnlockFragment, bundle);
                break;
            case R.id.btnParentShell:
                bundle.putBoolean("isClose", false);
                navController.navigate(R.id.kidShellFragment_to_selfUnlockFragment, bundle);
                break;
            default:
                break;
        }
        dismiss();
    }
}
