package com.floydwiz.pikapika.ui;

import android.os.Bundle;

import com.floydwiz.pikapika.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SelfUnlockActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_unlock);
    }
}
