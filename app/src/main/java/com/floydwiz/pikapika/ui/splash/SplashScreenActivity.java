package com.floydwiz.pikapika.ui.splash;


import android.content.Intent;
import android.os.Bundle;

import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.databinding.ActivitySplashScreenBinding;
import com.floydwiz.pikapika.ui.base.BaseActivity;
import com.floydwiz.pikapika.ui.onboarding.OnBoardingActivity;
import com.floydwiz.pikapika.utils.HomeUtil;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;

public class SplashScreenActivity extends BaseActivity<SplashViewModel, ActivitySplashScreenBinding> {

    @Override
    protected int provideLayout() {
        return R.layout.activity_splash_screen;
    }

    @Override
    protected Class<SplashViewModel> provideViewModelClass() {
        return SplashViewModel.class;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                handleNavigation();
            }
        }, 3000);
    }

    private void handleNavigation() {
        if (getViewModel().isUserRegistered()) {
            openLauncherActivity();
        } else {
            openOnBoardingActivity();
        }
    }

    public void openOnBoardingActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, OnBoardingActivity.class);
        startActivity(intent);
        finish();
    }

    public void openLauncherActivity() {
        HomeUtil.handleHomeButton(this);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
