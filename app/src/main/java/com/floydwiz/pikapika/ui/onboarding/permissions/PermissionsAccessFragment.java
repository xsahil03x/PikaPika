package com.floydwiz.pikapika.ui.onboarding.permissions;


import android.app.AppOpsManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.data.local.prefs.AppPreferencesHelper;
import com.floydwiz.pikapika.databinding.FragmentPermissionAccessBinding;
import com.floydwiz.pikapika.ui.base.BaseFragment;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class PermissionsAccessFragment extends BaseFragment<PermissionsViewModel, FragmentPermissionAccessBinding> {

    private static final int NOTIFICATION_REQUEST_CODE = 333;
    private static final int DRAW_OVER_REQUEST_CODE = 666;
    private static final int USAGE_ACCESS_REQUEST_CODE = 999;

    @Inject
    AppPreferencesHelper mHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int provideLayout() {
        return R.layout.fragment_permission_access;
    }

    @Override
    protected Class<PermissionsViewModel> provideViewModelClass() {
        return PermissionsViewModel.class;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final NavController navController = Navigation.findNavController(getDataBinding().getRoot());

        if (mHelper.getPermissionsState())
            navController.navigate(R.id.permissionsAccessFragment_to_appLockFragment);

        getDataBinding().setPermissionViewModel(getViewModel());

        checkIfPermissionsAlreadyAvailable();

        getDataBinding().switchUsageAccess.setOnTouchListener((v, event) -> {
            getDataBinding().switchUsageAccess.setClickable(false);
            if (!isUserAccessGranted()) {
                startActivityForResult(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS), USAGE_ACCESS_REQUEST_CODE);
            }
            return false;
        });

        getDataBinding().switchNotificationBlock.setOnTouchListener((v, event) -> {
            getDataBinding().switchNotificationBlock.setClickable(false);
            if (!isNotificationServiceGranted()) {
                startActivityForResult(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS), NOTIFICATION_REQUEST_CODE);
            }
            return false;
        });

        getDataBinding().switchDrawOverPermission.setOnTouchListener((v, event) -> {
            getDataBinding().switchDrawOverPermission.setClickable(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getContext().getPackageName()));
                startActivityForResult(intent, DRAW_OVER_REQUEST_CODE);
            }
            return false;
        });

        getViewModel().getButtonClick().observe(this, permissions -> {
            if (permissions.isPermissionsAvailable()) {
                mHelper.setPermissionsState(true);
                navController.navigate(R.id.permissionsAccessFragment_to_appLockFragment);
            }
        });
    }

    private void checkIfPermissionsAlreadyAvailable() {
        if (isNotificationServiceGranted()) {
            getViewModel().getSwitches().setNotificationAccess(true);
            getDataBinding().switchNotificationBlock.setClickable(false);
        }
        if (isUserAccessGranted()) {
            getViewModel().getSwitches().setUsageAccess(true);
            getDataBinding().switchUsageAccess.setClickable(false);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getContext())) {
            getViewModel().getSwitches().setDrawOverAccess(true);
            getDataBinding().switchDrawOverPermission.setClickable(false);
        }
    }

    private boolean isNotificationServiceGranted() {
        ContentResolver contentResolver = getContext().getContentResolver();
        String enabledNotificationListeners =
                Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        String packageName = getContext().getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }

    private boolean isUserAccessGranted() {
        try {
            PackageManager packageManager = getContext().getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getContext().getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) getContext().getSystemService(Context.APP_OPS_SERVICE);
            int mode;
            mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                    applicationInfo.uid, applicationInfo.packageName);
            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case USAGE_ACCESS_REQUEST_CODE:
                if (isUserAccessGranted())
                    getDataBinding().switchUsageAccess.setChecked(true);
                else {
                    getDataBinding().switchUsageAccess.setClickable(true);
                    getDataBinding().switchUsageAccess.setChecked(false);
                }
                break;
            case NOTIFICATION_REQUEST_CODE:
                if (isNotificationServiceGranted())
                    getDataBinding().switchNotificationBlock.setChecked(true);
                else {
                    getDataBinding().switchNotificationBlock.setClickable(true);
                    getDataBinding().switchNotificationBlock.setChecked(false);
                }
                break;
            case DRAW_OVER_REQUEST_CODE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(getContext()))
                    getDataBinding().switchDrawOverPermission.setChecked(true);
                else {
                    getDataBinding().switchDrawOverPermission.setClickable(true);
                    getDataBinding().switchDrawOverPermission.setChecked(false);
                }
        }
    }
}

