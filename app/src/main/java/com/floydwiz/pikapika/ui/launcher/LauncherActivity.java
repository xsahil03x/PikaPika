package com.floydwiz.pikapika.ui.launcher;

import android.os.Bundle;

import com.floydwiz.pikapika.R;
import com.floydwiz.pikapika.databinding.ActivityLauncherBinding;
import com.floydwiz.pikapika.ui.base.BaseActivity;

public class LauncherActivity extends BaseActivity<LauncherViewModel, ActivityLauncherBinding> {

    @Override
    protected int provideLayout() {
        return R.layout.activity_launcher;
    }

    @Override
    protected Class<LauncherViewModel> provideViewModelClass() {
        return LauncherViewModel.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}
