package com.floydwiz.pikapika.ui.onboarding.permissions;

import android.widget.CompoundButton;

import com.floydwiz.pikapika.data.models.PermissionSwitches;
import com.floydwiz.pikapika.ui.base.BaseViewModel;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;

public class PermissionsViewModel extends BaseViewModel {

    private PermissionSwitches switches;
    private CompoundButton.OnCheckedChangeListener onCheckedUsageAccess;
    private CompoundButton.OnCheckedChangeListener onCheckedNotificationAccess;
    private CompoundButton.OnCheckedChangeListener onCheckedDrawOverAccess;
    private MutableLiveData<PermissionSwitches> buttonClick = new MutableLiveData<>();

    @Inject
    public PermissionsViewModel() {
        init();
    }

    private void init() {
        switches = new PermissionSwitches();

        onCheckedUsageAccess = (buttonView, isChecked) -> {
            if (isChecked) switches.setUsageAccess(true);
        };

        onCheckedNotificationAccess = (buttonView, isChecked) -> {
            if (isChecked) switches.setNotificationAccess(true);
        };

        onCheckedDrawOverAccess = (buttonView, isChecked) -> {
            if (isChecked) switches.setDrawOverAccess(true);
        };
    }

    public PermissionSwitches getSwitches() {
        return switches;
    }

    public CompoundButton.OnCheckedChangeListener getOnCheckedUsageAccess() {
        return onCheckedUsageAccess;
    }

    public CompoundButton.OnCheckedChangeListener getOnCheckedNotificationAccess() {
        return onCheckedNotificationAccess;
    }

    public CompoundButton.OnCheckedChangeListener getOnCheckedDrawOverAccess() {
        return onCheckedDrawOverAccess;
    }

    public void onButtonClick() {
        if (switches.isPermissionsAvailable()) {
            buttonClick.setValue(switches);
        }
    }

    public MutableLiveData<PermissionSwitches> getButtonClick() {
        return buttonClick;
    }

}
