package com.floydwiz.pikapika.ui.onboarding.profiling;

import android.view.View;
import android.widget.EditText;

import com.floydwiz.pikapika.data.UserRepository;
import com.floydwiz.pikapika.data.models.ProfilingFields;
import com.floydwiz.pikapika.data.models.User;
import com.floydwiz.pikapika.ui.base.BaseViewModel;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class ProfilingViewModel extends BaseViewModel {

    private ProfilingFields profilingFields;
    private View.OnFocusChangeListener onFocusParentEmail;
    private View.OnFocusChangeListener onFocusChildName;
    private View.OnFocusChangeListener onFocusChildAge;
    private MutableLiveData<ProfilingFields> buttonClick = new MutableLiveData<>();

    private UserRepository userRepository;

    @Inject
    ProfilingViewModel(UserRepository repository) {
        this.userRepository = repository;
        init();
    }

    LiveData<Integer> createNewUser(User user) {
        return userRepository.createNewUser(user);
    }

    private void init() {
        profilingFields = new ProfilingFields();
        onFocusParentEmail = (view, focused) -> {
            EditText et = (EditText) view;
            if (et.getText().length() > 0 && !focused) {
                profilingFields.isEmailValid(true);
            }
        };

        onFocusChildName = (view, focused) -> {
            EditText et = (EditText) view;
            if (et.getText().length() > 0 && !focused) {
                profilingFields.isNameValid(true);
            }
        };

        onFocusChildAge = (view, focused) -> {
            EditText et = (EditText) view;
            if (et.getText().length() > 0 && !focused) {
                profilingFields.isAgeValid(true);
            }
        };
    }

    public ProfilingFields getProfilingFields() {
        return profilingFields;
    }

    public View.OnFocusChangeListener getParentEmailOnFocusChangeListener() {
        return onFocusParentEmail;
    }

    public View.OnFocusChangeListener getChildNameOnFocusChangeListener() {
        return onFocusChildName;
    }

    public View.OnFocusChangeListener getChildAgeOnFocusChangeListener() {
        return onFocusChildAge;
    }

    public void onButtonClick() {
        if (profilingFields.isValid()) {
            buttonClick.setValue(profilingFields);
        }
    }

    MutableLiveData<ProfilingFields> getButtonClick() {
        return buttonClick;
    }
}
