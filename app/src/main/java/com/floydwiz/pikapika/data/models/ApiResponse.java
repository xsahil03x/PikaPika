package com.floydwiz.pikapika.data.models;

import com.squareup.moshi.Json;

public class ApiResponse {

    @Json(name = "statusCode")
    private Integer statusCode;
    @Json(name = "body")
    private User user;
    @Json(name = "isBase64Encoded")
    private boolean isBase64Encoded;

    public ApiResponse(Integer statusCode, User user, boolean isBase64Encoded) {
        this.statusCode = statusCode;
        this.user = user;
        this.isBase64Encoded = isBase64Encoded;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isBase64Encoded() {
        return isBase64Encoded;
    }

    public void setBase64Encoded(boolean base64Encoded) {
        isBase64Encoded = base64Encoded;
    }
}

