package com.floydwiz.pikapika.data;

import com.floydwiz.pikapika.data.models.AllowedApp;

import java.util.List;

import androidx.lifecycle.LiveData;

public interface AppRepository {

    LiveData<List<AllowedApp>> getAllowedAppList();

    LiveData<List<AllowedApp>> getAllAppList();

    void handleSelectedApp(AllowedApp app);
}
