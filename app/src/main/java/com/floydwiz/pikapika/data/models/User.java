package com.floydwiz.pikapika.data.models;

import com.squareup.moshi.Json;

public class User {

    @Json(name = "userId")
    private String userId;
    @Json(name = "childName")
    private String childName;
    @Json(name = "childAge")
    private String childAge;
    @Json(name = "parentEmail")
    private String parentEmail;

    public User(String userId, String childName, String childAge, String parentEmail) {
        this.userId = userId;
        this.childName = childName;
        this.childAge = childAge;
        this.parentEmail = parentEmail;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getChildAge() {
        return childAge;
    }

    public void setChildAge(String childAge) {
        this.childAge = childAge;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }
}
