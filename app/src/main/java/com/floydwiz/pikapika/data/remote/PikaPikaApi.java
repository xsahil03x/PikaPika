package com.floydwiz.pikapika.data.remote;

import com.floydwiz.pikapika.data.models.ApiResponse;
import com.floydwiz.pikapika.data.models.User;

import androidx.lifecycle.LiveData;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface PikaPikaApi {

    @GET("users")
    Single<ApiResponse> fetchUserInfo(@Query("userId") String userId);

    @POST("users")
    Single<ApiResponse> createNewUser(@Body User user);

}