package com.floydwiz.pikapika.data.models;

import android.view.View;
import android.widget.EditText;

import com.floydwiz.pikapika.BR;
import com.floydwiz.pikapika.R;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

public class ProfilingFields extends BaseObservable {

    private String childName;
    private String childAge;
    private String parentEmail;
    public ObservableField<Integer> childNameError = new ObservableField<>();
    public ObservableField<Integer> childAgeError = new ObservableField<>();
    public ObservableField<Integer> parentEmailError = new ObservableField<>();

    @Bindable
    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
        notifyPropertyChanged(BR.valid);
    }

    @Bindable
    public String getChildAge() {
        return childAge;
    }

    public void setChildAge(String childAge) {
        this.childAge = childAge;
        notifyPropertyChanged(BR.valid);
    }

    @Bindable
    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
        notifyPropertyChanged(BR.valid);
    }

    @Bindable
    public boolean isValid() {
        return isEmailValid(true)
                && isAgeValid(true)
                && isNameValid(true);
    }

    public boolean isEmailValid(boolean setMessage) {
        if (parentEmail != null && parentEmail.length() > 5) {
            int indexOfAt = parentEmail.indexOf("@");
            int indexOfDot = parentEmail.lastIndexOf(".");
            if (indexOfAt > 0 && indexOfDot > indexOfAt && indexOfDot < parentEmail.length() - 1) {
                parentEmailError.set(null);
                return true;
            } else {
                if (setMessage)
                    parentEmailError.set(R.string.error_parent_email);
                return false;
            }
        }
        return false;
    }

    public boolean isAgeValid(boolean setMessage) {
        if (childAge != null && !childAge.isEmpty()) {
            childAgeError.set(null);
            return true;
        } else {
            if (setMessage)
                childAgeError.set(R.string.error_child_age);
            return false;
        }
    }

    public boolean isNameValid(boolean setMessage) {
        if (childName != null && !childName.isEmpty()) {
            childNameError.set(null);
            return true;
        } else {
            if (setMessage)
                childNameError.set(R.string.error_child_name);
            return false;
        }
    }

    @BindingAdapter("android:error")
    public static void setError(EditText editText, Object strOrResId) {
        if (strOrResId instanceof Integer) {
            editText.setError(editText.getContext().getString((Integer) strOrResId));
        } else {
            editText.setError((String) strOrResId);
        }
    }

    @BindingAdapter("android:onFocus")
    public static void bindFocusChange(EditText editText, View.OnFocusChangeListener onFocusChangeListener) {
        if (editText.getOnFocusChangeListener() == null) {
            editText.setOnFocusChangeListener(onFocusChangeListener);
        }
    }
}
