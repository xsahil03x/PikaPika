package com.floydwiz.pikapika.data.local.db;

import com.floydwiz.pikapika.data.models.AllowedApp;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {AllowedApp.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "AppDatabase";

    public abstract AppDao appDao();
}
