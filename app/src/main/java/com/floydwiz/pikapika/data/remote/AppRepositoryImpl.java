package com.floydwiz.pikapika.data.remote;

import com.floydwiz.pikapika.data.AppRepository;
import com.floydwiz.pikapika.data.local.db.AppDao;
import com.floydwiz.pikapika.data.models.AllowedApp;
import com.floydwiz.pikapika.utils.ApkInfoExtractor;
import com.floydwiz.pikapika.utils.PikaPikaLogger;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class AppRepositoryImpl implements AppRepository {

    private CompositeDisposable mDisposable;
    private AppDao appDao;
    private ApkInfoExtractor extractor;

    @Inject
    public AppRepositoryImpl(AppDao appDao, ApkInfoExtractor extractor) {
        this.appDao = appDao;
        this.mDisposable = new CompositeDisposable();
        this.extractor = extractor;
    }

    @Override
    public LiveData<List<AllowedApp>> getAllowedAppList() {
        final MutableLiveData<List<AllowedApp>> appList = new MutableLiveData<>();
        mDisposable.add(appDao.getAllAllowedApps()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSubscriber<List<AllowedApp>>() {
                    @Override
                    public void onNext(List<AllowedApp> allowedAppsList) {
                        appList.postValue(allowedAppsList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        PikaPikaLogger.e(e, e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        PikaPikaLogger.i("getAllowedAppListCompleted");
                    }
                }));
        return appList;
    }

    @Override
    public LiveData<List<AllowedApp>> getAllAppList() {

        final MutableLiveData<List<AllowedApp>> appList = new MutableLiveData<>();

        Flowable<List<AllowedApp>> allAllowedApps = appDao.getAllAllowedApps();

        Flowable<List<AllowedApp>> allInstalledApkInfo = extractor.getAllInstalledApkInfo().map(packageNames -> {
            List<AllowedApp> apps = new ArrayList<>();
            for (String packageName : packageNames) {
                apps.add(new AllowedApp(extractor.getAppName(packageName), packageName, false));
            }
            return apps;
        });

        Flowable<List<AllowedApp>> apps = Flowable.zip(allAllowedApps, allInstalledApkInfo, (allAllowedApps1, allInstalledApkInfo1) -> {
            for (AllowedApp app : allInstalledApkInfo1) {
                for (AllowedApp app2 : allAllowedApps1) {
                    if (app.getPackageName().equalsIgnoreCase(app2.getPackageName()))
                        app.setAllowed(app2.isAllowed());
                }
            }
            return allInstalledApkInfo1;
        });

        mDisposable.add(apps
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSubscriber<List<AllowedApp>>() {
                    @Override
                    public void onNext(List<AllowedApp> allowedAppsList) {
                        appList.postValue(allowedAppsList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        PikaPikaLogger.e(e, e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        PikaPikaLogger.i("getAllAppListCompleted");
                    }
                }));

        return appList;
    }

    @Override
    public void handleSelectedApp(AllowedApp app) {
        final boolean allowed = !app.isAllowed();
        if (allowed) {
            app.setAllowed(true);
            mDisposable.add(Completable.fromAction(() -> appDao.insertAppToKidShell(app))
                    .subscribeOn(Schedulers.io())
                    .subscribe());
        } else {
            app.setAllowed(false);
            mDisposable.add(Completable.fromAction(() -> appDao.deleteAppFromKidShell(app.getPackageName()))
                    .subscribeOn(Schedulers.io())
                    .subscribe());
        }
    }
}
