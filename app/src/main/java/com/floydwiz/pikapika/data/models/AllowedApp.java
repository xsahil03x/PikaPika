package com.floydwiz.pikapika.data.models;

import android.widget.ImageView;

import com.floydwiz.pikapika.di.modules.GlideApp;
import com.floydwiz.pikapika.utils.ApkInfoExtractor;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "AllowedApp")
public class AllowedApp {

    @NonNull
    @PrimaryKey
    private String packageName;

    private String name;

    private boolean isAllowed = false;

    @Ignore
    public AllowedApp() {
    }

    public AllowedApp(String name, @NonNull String packageName, Boolean isAllowed) {
        this.name = name;
        this.packageName = packageName;
        this.isAllowed = isAllowed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NonNull
    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(@NonNull String packageName) {
        this.packageName = packageName;
    }

    public boolean isAllowed() {
        return isAllowed;
    }

    public void setAllowed(boolean allowed) {
        isAllowed = allowed;
    }
}
