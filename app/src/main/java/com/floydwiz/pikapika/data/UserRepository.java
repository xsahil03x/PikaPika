package com.floydwiz.pikapika.data;

import com.floydwiz.pikapika.data.models.User;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

public interface UserRepository {

    LiveData<User> getUserInfo(@NonNull String userId);

    LiveData<Integer> createNewUser(@NonNull User user);

}
