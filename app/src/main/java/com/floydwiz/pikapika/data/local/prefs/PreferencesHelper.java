package com.floydwiz.pikapika.data.local.prefs;

public interface PreferencesHelper {

    void setUserLoggedIn(boolean isLoggedIn);

    boolean getUserLoggedIn();

    void setSelfLockState(boolean isLocked);

    boolean getSelfLockState();

    void setPermissionsState(boolean isAvailable);

    boolean getPermissionsState();
}
