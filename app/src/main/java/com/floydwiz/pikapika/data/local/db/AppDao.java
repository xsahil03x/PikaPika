package com.floydwiz.pikapika.data.local.db;

import com.floydwiz.pikapika.data.models.AllowedApp;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Flowable;

@Dao
public interface AppDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAppToKidShell(AllowedApp app);

    @Query("SELECT * FROM AllowedApp")
    Flowable<List<AllowedApp>> getAllAllowedApps();

    @Query("DELETE FROM AllowedApp WHERE packageName = :appPackageName")
    void deleteAppFromKidShell(String appPackageName);

}
