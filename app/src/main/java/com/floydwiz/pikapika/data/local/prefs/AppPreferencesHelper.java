package com.floydwiz.pikapika.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.floydwiz.pikapika.utils.AppConstants;

import javax.inject.Inject;

public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_USER_LOGGED_IN = "PREF_USER_LOGGED_IN";
    private static final String PREF_SELF_LOCK_STATE = "PREF_SELF_LOCK_STATE";
    private static final String PREF_PERMISSIONS_AVAILABLE = "PREF_PERMISSIONS_AVAILABLE";

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(SharedPreferences mPrefs) {
        this.mPrefs = mPrefs;
    }

    @Override
    public void setUserLoggedIn(boolean isLoggedIn) {
        mPrefs.edit().putBoolean(PREF_USER_LOGGED_IN, isLoggedIn).apply();
    }

    @Override
    public boolean getUserLoggedIn() {
        return mPrefs.getBoolean(PREF_USER_LOGGED_IN, false);
    }

    @Override
    public void setSelfLockState(boolean isLocked) {
        mPrefs.edit().putBoolean(PREF_SELF_LOCK_STATE, isLocked).apply();
    }

    @Override
    public boolean getSelfLockState() {
        return mPrefs.getBoolean(PREF_SELF_LOCK_STATE, false);
    }

    @Override
    public void setPermissionsState(boolean isAvailable) {
        mPrefs.edit().putBoolean(PREF_PERMISSIONS_AVAILABLE, isAvailable).apply();
    }

    @Override
    public boolean getPermissionsState() {
        return mPrefs.getBoolean(PREF_PERMISSIONS_AVAILABLE, false);
    }
}
