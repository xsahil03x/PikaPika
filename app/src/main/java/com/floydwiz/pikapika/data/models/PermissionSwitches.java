package com.floydwiz.pikapika.data.models;

import com.floydwiz.pikapika.BR;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class PermissionSwitches extends BaseObservable {

    private boolean usageAccess;
    private boolean drawOverAccess;
    private boolean notificationAccess;

    @Bindable
    public boolean isPermissionsAvailable() {
        return isNotificationAccess()
                && isDrawOverAccess()
                && isUsageAccess();
    }

    @Bindable
    public boolean isUsageAccess() {
        return usageAccess;
    }

    public void setUsageAccess(boolean usageAccess) {
        this.usageAccess = usageAccess;
        notifyPropertyChanged(BR.permissionsAvailable);
    }

    @Bindable
    public boolean isDrawOverAccess() {
        return drawOverAccess;
    }

    public void setDrawOverAccess(boolean drawOverAccess) {
        this.drawOverAccess = drawOverAccess;
        notifyPropertyChanged(BR.permissionsAvailable);
    }

    @Bindable
    public boolean isNotificationAccess() {
        return notificationAccess;
    }

    public void setNotificationAccess(boolean notificationAccess) {
        this.notificationAccess = notificationAccess;
        notifyPropertyChanged(BR.permissionsAvailable);
    }
}
