package com.floydwiz.pikapika.data.remote;

import com.floydwiz.pikapika.data.UserRepository;
import com.floydwiz.pikapika.data.models.ApiResponse;
import com.floydwiz.pikapika.data.models.User;
import com.floydwiz.pikapika.utils.PikaPikaLogger;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class UserRepositoryImpl implements UserRepository {

    private PikaPikaApi pikaPikaApi;
    private CompositeDisposable mDisposable;

    @Inject
    public UserRepositoryImpl(PikaPikaApi pikaPikaApi) {
        this.pikaPikaApi = pikaPikaApi;
        this.mDisposable = new CompositeDisposable();
    }

    @Override
    public LiveData<User> getUserInfo(@NonNull String userId) {
        final MutableLiveData<User> user = new MutableLiveData<>();
        mDisposable.add(pikaPikaApi.fetchUserInfo(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ApiResponse>() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        user.postValue(response.getUser());
                    }

                    @Override
                    public void onError(Throwable e) {
                        PikaPikaLogger.e(e, e.getMessage());
                    }
                }));
        return user;
    }

    @Override
    public LiveData<Integer> createNewUser(@NonNull User user) {
        final MutableLiveData<Integer> statusCode = new MutableLiveData<>();
        mDisposable.add(pikaPikaApi.createNewUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ApiResponse>() {
                    @Override
                    public void onSuccess(ApiResponse response) {
                        statusCode.postValue(response.getStatusCode());
                    }

                    @Override
                    public void onError(Throwable e) {
                        PikaPikaLogger.e(e, e.getMessage());
                    }
                }));
        return statusCode;
    }
}
