package com.floydwiz.pikapika.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;

import com.floydwiz.pikapika.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.core.content.ContextCompat;
import io.reactivex.Flowable;

public class ApkInfoExtractor {

    private Context context;

    @Inject
    public ApkInfoExtractor(Context context) {
        this.context = context;
    }

    /**
     * Method to get the information of all the installed apps
     *
     * @return - returns a List<String> containing all the info
     */
    public Flowable<List<String>> getAllInstalledApkInfo() {

        List<String> ApkPackageName = new ArrayList<>();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities(intent, 0);

        for (ResolveInfo resolveInfo : resolveInfoList) {

            ActivityInfo activityInfo = resolveInfo.activityInfo;

            if (!isSystemPackage(resolveInfo) && !isPikaPika(resolveInfo)) {

                ApkPackageName.add(activityInfo.applicationInfo.packageName);
            }
        }

        return Flowable.fromArray(ApkPackageName);

    }

    private boolean isPikaPika(ResolveInfo resolveInfo) {
        return resolveInfo.activityInfo.packageName.equals(context.getPackageName());
    }

    /**
     * Method to check if the app is System or Third Party
     *
     * @param resolveInfo - Info about the app
     * @return boolean - Returns true if the app is third Party
     */
    private boolean isSystemPackage(ResolveInfo resolveInfo) {

        return ((resolveInfo.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }

    /**
     * Method to retrieve the app Icons
     *
     * @param ApkTempPackageName - Package name of the app
     * @return Drawable - Returns a Drawable of app Icon
     */
    public Drawable getAppIconByPackageName(String ApkTempPackageName) {

        Drawable drawable;

        try {
            drawable = context.getPackageManager().getApplicationIcon(ApkTempPackageName);

        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();

            drawable = ContextCompat.getDrawable(context, R.mipmap.ic_launcher);
        }
        return drawable;
    }

    /**
     * Method to get the App Name
     *
     * @param ApkPackageName - Package name of the app
     * @return String - Returns the App name
     */
    public String getAppName(String ApkPackageName) {

        String Name = "";

        ApplicationInfo applicationInfo;

        PackageManager packageManager = context.getPackageManager();

        try {

            applicationInfo = packageManager.getApplicationInfo(ApkPackageName, 0);

            if (applicationInfo != null) {

                Name = (String) packageManager.getApplicationLabel(applicationInfo);
            }

        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();
        }
        return Name;
    }
}
