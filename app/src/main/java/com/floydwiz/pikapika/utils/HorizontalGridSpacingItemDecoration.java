package com.floydwiz.pikapika.utils;

import android.graphics.Rect;
import android.view.View;

import org.jetbrains.annotations.NotNull;

import androidx.recyclerview.widget.RecyclerView;

public class HorizontalGridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private final int spanCount;
    private final int spacing;
    private final boolean includeEdge;

    public HorizontalGridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
    }

    @Override
    public void getItemOffsets(@NotNull Rect outRect, @NotNull View view, @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int row = position % spanCount; // item column

        if (includeEdge) {
            outRect.top = spacing - row * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.bottom = (row) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

            if (position < spanCount) { // top edge
                outRect.left = spacing;
            }
            outRect.right = spacing * 2; // item bottom
        } else {
            outRect.left = row * spacing / spanCount; // column * ((1f / spanCount) * spacing)
            outRect.right = spacing - (row + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) {
                outRect.top = spacing; // item top
            }
        }
    }
}
