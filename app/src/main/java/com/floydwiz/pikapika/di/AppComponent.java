package com.floydwiz.pikapika.di;

import android.app.Application;

import com.floydwiz.pikapika.PikaPikaApp;
import com.floydwiz.pikapika.di.modules.ActivityBindingModule;
import com.floydwiz.pikapika.di.modules.AppModule;
import com.floydwiz.pikapika.di.modules.ViewModelFactoryModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, ActivityBindingModule.class,
        AppModule.class, ViewModelFactoryModule.class})
interface AppComponent extends AndroidInjector<PikaPikaApp> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}