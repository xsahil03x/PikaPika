package com.floydwiz.pikapika.di.modules;

import com.floydwiz.pikapika.di.scopes.PerActivity;
import com.floydwiz.pikapika.di.scopes.PerFragment;
import com.floydwiz.pikapika.ui.launcher.LauncherActivity;
import com.floydwiz.pikapika.ui.launcher.kid.KidShellFragment;
import com.floydwiz.pikapika.ui.launcher.parent.ParentFragment;
import com.floydwiz.pikapika.ui.onboarding.OnBoardingActivity;
import com.floydwiz.pikapika.ui.onboarding.applock.AppLockFragment;
import com.floydwiz.pikapika.ui.onboarding.permissions.PermissionsAccessFragment;
import com.floydwiz.pikapika.ui.onboarding.profiling.KidProfilingFragment;
import com.floydwiz.pikapika.ui.splash.SplashScreenActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    // Activities

    @PerActivity
    @ContributesAndroidInjector()
    public abstract SplashScreenActivity bindSplashScreenActivity();

    @PerActivity
    @ContributesAndroidInjector()
    public abstract OnBoardingActivity bindOnBoardingActivity();

    @PerActivity
    @ContributesAndroidInjector()
    public abstract LauncherActivity bindLauncherActivity();

    // Fragments
    @PerFragment
    @ContributesAndroidInjector()
    public abstract KidProfilingFragment bindKidProfilingFragment();

    @PerFragment
    @ContributesAndroidInjector()
    public abstract PermissionsAccessFragment bindPermissionsAccessFragment();

    @PerFragment
    @ContributesAndroidInjector()
    public abstract AppLockFragment bindAppLockFragment();

    @PerFragment
    @ContributesAndroidInjector()
    public abstract ParentFragment bindParentFragment();

    @PerFragment
    @ContributesAndroidInjector()
    public abstract KidShellFragment bindKidShellFragment();


    // Services

   /* @PerService
    @ContributesAndroidInjector()
    public abstract FetchMemeTemplatesJob bindFetchMemeTemplateJob();*/

    // Broadcast Receivers

/*    @PerBroadcastReciever
    @ContributesAndroidInjector()
    public abstract MemeWidget bindMemeWidget();*/
}
