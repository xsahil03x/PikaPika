package com.floydwiz.pikapika.di.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.floydwiz.pikapika.data.AppRepository;
import com.floydwiz.pikapika.data.UserRepository;
import com.floydwiz.pikapika.data.local.db.AppDao;
import com.floydwiz.pikapika.data.local.db.AppDatabase;
import com.floydwiz.pikapika.data.local.prefs.AppPreferencesHelper;
import com.floydwiz.pikapika.data.remote.AppRepositoryImpl;
import com.floydwiz.pikapika.data.remote.PikaPikaApi;
import com.floydwiz.pikapika.data.remote.UserRepositoryImpl;
import com.floydwiz.pikapika.utils.ApkInfoExtractor;
import com.floydwiz.pikapika.utils.AppConstants;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public abstract class AppModule {

    private static final String BASE_URL = "https://aj5ksnxe3h.execute-api.ap-south-1.amazonaws.com/v1/";

    @Provides
    @Singleton
    static Resources provideResources(Application app) {
        return app.getResources();
    }

    @Provides
    @Singleton
    static AppPreferencesHelper provideAppPrefsHelper(SharedPreferences preferences) {
        return new AppPreferencesHelper(preferences);
    }

    @Provides
    @Singleton
    static SharedPreferences provideSharedPreferences(Application app) {
        return app.getApplicationContext()
                .getSharedPreferences(AppConstants.PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    static ApkInfoExtractor provideApkInfoExtractor(Application app) {
        return new ApkInfoExtractor(app.getApplicationContext());
    }

    @Provides
    @Singleton
    static Retrofit provideRetrofit(
            MoshiConverterFactory moshiCon,
            OkHttpClient client,
            RxJava2CallAdapterFactory rxCallAdapter) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(moshiCon)
                .addCallAdapterFactory(rxCallAdapter)
                .build();
    }

    @Provides
    @Singleton
    static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    static RxJava2CallAdapterFactory provideRxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @Singleton
    static MoshiConverterFactory provideMoshiConverterFactory() {
        return MoshiConverterFactory.create();
    }

    @Provides
    @Singleton
    static PikaPikaApi providePikaPikaApi(Retrofit retrofit) {
        return retrofit.create(PikaPikaApi.class);
    }

    @Provides
    @Singleton
    static AppDatabase provideAppDatabase(Application app) {
        return Room.databaseBuilder(app, AppDatabase.class, AppDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    static AppDao provideAppDao(AppDatabase appDatabase) {
        return appDatabase.appDao();
    }

    @Provides
    @Singleton
    static UserRepository provideUserRepository(PikaPikaApi api) {
        return new UserRepositoryImpl(api);
    }

    @Provides
    @Singleton
    static AppRepository provideAppRepository(AppDao appDao, ApkInfoExtractor extractor) {
        return new AppRepositoryImpl(appDao, extractor);
    }

/*    @Provides
    @Singleton
    static DatabaseReference provideDatabaseReference() {
        return FirebaseDatabase.getInstance().getReference();
    }*/

/*    @Provides
    @Singleton
    static StorageReference provideStorageReference() {
        return FirebaseStorage.getInstance().getReference();
    }*/

}