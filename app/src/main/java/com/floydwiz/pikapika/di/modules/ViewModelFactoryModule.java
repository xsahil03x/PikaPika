package com.floydwiz.pikapika.di.modules;

import com.floydwiz.pikapika.ui.base.ViewModelFactory;
import com.floydwiz.pikapika.ui.launcher.LauncherViewModel;
import com.floydwiz.pikapika.ui.onboarding.permissions.PermissionsViewModel;
import com.floydwiz.pikapika.ui.onboarding.profiling.ProfilingViewModel;
import com.floydwiz.pikapika.ui.splash.SplashViewModel;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.MapKey;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@MapKey
@interface ViewModelKey {
    Class<? extends ViewModel> value();
}

@Module
public abstract class ViewModelFactoryModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    abstract ViewModel bindSplashScreenViewModel(SplashViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ProfilingViewModel.class)
    abstract ViewModel bindProfilingViewModel(ProfilingViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PermissionsViewModel.class)
    abstract ViewModel bindPermissionsViewModel(PermissionsViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LauncherViewModel.class)
    abstract ViewModel bindLauncherViewModel(LauncherViewModel viewModel);

}