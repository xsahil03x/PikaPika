package com.floydwiz.pikapika.di.scopes;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
