package com.floydwiz.pikapika.di.modules;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

import androidx.annotation.NonNull;

@GlideModule
public class PikaPikaAppGlideModule extends AppGlideModule {

    @Override
    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull
            Registry registry) {
/*        registry.append(StorageReference.class, InputStream.class,
                new FirebaseImageLoader.Factory());*/
    }
}
