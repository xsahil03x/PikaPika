package com.floydwiz.lockpatternview;

import android.content.Context;

import com.floydwiz.lockpatternview.base.BasePresenter;
import com.floydwiz.lockpatternview.base.BaseView;

import java.util.List;

public interface MainContract {

    interface View extends BaseView<Presenter> {
        void loadAppInfoSuccess(List<CommLockInfo> list);
    }

    interface Presenter extends BasePresenter {
        void loadAppInfo(Context context, boolean isSort);

        void loadLockAppInfo(Context context);

        void onDestroy();
    }
}
